# Jeu de carte

Ce jeu de carte est une simple application [Spring Boot](http://projects.spring.io/spring-boot/).

Descriptive:

Dans ce jeu, un joueur tire une main de 10 cartes de manière aléatoire.
Chaque carte possède une couleur ("Carreaux", par exemple) et une valeur ("10", par exemple).

On vous demande de:

Construire un ordre aléatoire des couleurs. L'ordre des couleurs est, par exemple, l'un des suivants :

--> Carreaux, Cœur, Pique, Trèfle

Construire un ordre aléatoire des valeurs.
L'ordre des valeurs est, par exemple, l'un des suivants :

--> As, 5, 10, 8, 6, 7, 4, 2, 3, 9, Dame, Roi, Valet

Construire une main de 10 cartes de manière aléatoire.

Présenter la main "non triée" à l'écran puis la main triée selon n'importe quel ordre défini dans la 1ère et 2ème étape. C'est-à-dire que vous devez classer les cartes par couleur et valeur.


## prérequis

Pour builder et lancer l'application on aura besoin de:

- [JDK 11](https://jdk.java.net/11/)
- [Maven 3](https://maven.apache.org)

## Builder le project

```shell
mvn clean install
```

## Lancer localement l'application

Il existe plusieurs façons pour exécuter une application Spring Boot sur votre ordinateur local. Une façon est d'exécuter la méthode `main` dans la classe  `fr/neurostack/card/CardGameApplication.java` depuis votre IDE.

Vous pouvez également utiliser le [plugin Spring Boot Maven](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) comme suit :

```shell
mvn spring-boot:run
```
L'application web est accessible via localhost:8080/

Les API suivants sont exposées :


1- Pour retourner une liste aléatoire de couleurs : [GET] http://localhost:8080/game/color

2- Pour retourner un ordre aléatoire de valeurs : [GET] http://localhost:8080/game/rank

3- Pour construire une main de `nombre` cartes de manière aléatoire : [GET] http://localhost:8080/game/card?numberOfCards=nombre

4- Pour ordonner les cartes : [POST] http://localhost:8080/game/card?colors='colors'&ranks='ranks' avec la liste de cartes à trier dans le body de la requêtes


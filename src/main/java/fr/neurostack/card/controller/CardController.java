package fr.neurostack.card.controller;


import fr.neurostack.card.model.*;
import fr.neurostack.card.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/game")
@CrossOrigin(origins = "*")
public class CardController {

    @Autowired
    private CardService cardService;

    @GetMapping(path = "/color")
    public List<Color> getColors(){
        return cardService.getColors();
    }

    @GetMapping(path = "/rank")
    public List<Rank> getRanks(){
        return cardService.getRanks();
    }

    @PostMapping(path = "/card")
    public List<Card> getDeckOfCard(@RequestParam List<Color> colors,
                                    @RequestParam List<Rank> ranks,
                                    @RequestBody List<Card> cardToSort){
        if (colors != null && ranks != null && cardToSort !=null){
            return cardService.getSortedDeckOfCard(colors,ranks, cardToSort);
        }
        throw new IllegalArgumentException("Missing Parameters");
    }

    @GetMapping(path = "/card")
    public List<Card> getDeckOfCard(@RequestParam Integer numberOfCards){
        return cardService.getDeckOfCard(numberOfCards);
    }

}

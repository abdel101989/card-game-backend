package fr.neurostack.card.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * {@link java.util.Enumeration} containing The list of Card's Ranks
 */
public enum Rank {
    DEUX(2),
    TROIS(3),
    QUATRE(4),
    CINQ(5),
    SIX(6),
    SEPT(7),
    HUIT(8),
    NEUF(9),
    DIX(10),
    VALET(11),
    REINE(12),
    ROI(13),
    AS(1);

    private final int value;

    Rank(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    /**
     *
     * @return Random list of Ranks
     */
    public static List<Rank> getRandomListOfRanks(){
        List<Rank> res =  Arrays.asList(Rank.values());
        Collections.shuffle(res);
        return res;
    }

}

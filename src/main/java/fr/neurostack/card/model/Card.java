package fr.neurostack.card.model;

public class Card {

    private Rank rank;
    private Color color;

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Card(Rank rank, Color color) {
        this.rank = rank;
        this.color = color;
    }
}

package fr.neurostack.card.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DeckOfCards {
    private List<Card> cards;
    private Integer numberOfCards;

    /**
     * Default Constructor contains a shuffled list of all cards
     */
    public DeckOfCards() {
        for (var s : Rank.values()) {
            for (var r : Color.values()) {
                this.cards.add(new Card(s,r));
                this.numberOfCards++;
            }
        }
        // Shuffling cards
        Collections.shuffle(this.cards);
    }

    /**
     * Constructor contains shuffled list of @numberOfCards cards
     * @param numberOfCards
     */
    public DeckOfCards(Integer numberOfCards) {
        List<Card> listDeCard = new ArrayList<Card>();
        this.numberOfCards = numberOfCards;
        for (var r : Rank.values()) {
            for (var c : Color.values()) {
                listDeCard.add(new Card(r,c));
            }
        }
        // Shuffling cards
        Collections.shuffle(listDeCard);
        // get @numberOfCards random cards
        this.cards = listDeCard.subList(0, numberOfCards);
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    /**
     * This methode allow sorting a deck of cards
     * based on an order of colors then an order of cards
     *
     * @param colors : list of color's order
     * @param ranks : list of rank's order
     * @param cardToSort : Deck of cards to be sorted
     * @return Deck of sorted cards
     */
    public static List<Card> sortCards(List<Color> colors, List<Rank> ranks, List<Card> cardToSort){
        List<Card> sortedCards = new ArrayList<>();
        for (var color : colors) {
            List<Card> list = cardToSort.stream().filter(card ->
                    color.equals(card.getColor())).collect(Collectors.toList());
            if (list == null || list.isEmpty())
                continue;
            for (var rank : ranks) {
                for (var card : list) {
                    if (rank.equals(card.getRank())){
                        sortedCards.add(card);
                    }
                }
            }
        }
        return sortedCards;
    }

}
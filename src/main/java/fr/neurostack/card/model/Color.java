package fr.neurostack.card.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * {@link java.util.Enumeration} containing The list of Card's Colors
 */
public enum Color {
    CARREAUX,
    COEUR,
    PIQUE,
    TREFLE;

    /**
     *
     * @return Random list of card's color
     */
    public static List<Color> getRandomListOfColors(){
        List<Color> res =  Arrays.asList(Color.values());
        Collections.shuffle(res);
        return res;
    }
}

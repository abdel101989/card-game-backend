package fr.neurostack.card.service;

import fr.neurostack.card.model.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {

    public List<Color> getColors(){
       return Color.getRandomListOfColors();
    }

    public List<Rank> getRanks(){
        return Rank.getRandomListOfRanks();
    }

    public List<Card> getDeckOfCard(Integer numberOfCards){
        return new DeckOfCards(numberOfCards).getCards();
    }

    public List<Card> getSortedDeckOfCard(List<Color> colors, List<Rank> ranks, List<Card> cardToSort){
        return DeckOfCards.sortCards(colors,ranks, cardToSort);
    }
}

package fr.neurostack.card.service;

import fr.neurostack.card.model.Card;
import fr.neurostack.card.model.Color;
import fr.neurostack.card.model.Rank;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

/**
 * Integration Tests
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CardGameServiceTest {

    @Autowired
    CardService cardService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getSortedDeckOfCardTest() {
        List<Color> colors = cardService.getColors();
        assertEquals(4, colors.size());
        List<Rank> ranks = cardService.getRanks();
        List<Card> deckOfCard = cardService.getDeckOfCard(10);
        assertEquals(10, deckOfCard.size());
        List<Card> sortedDeckOfCard = cardService.getSortedDeckOfCard(colors, ranks, deckOfCard);
        boolean contains = true;
        for (Card card : sortedDeckOfCard) {
            contains = contains && deckOfCard.contains(card);
        }
        assertTrue(contains);
    }
}
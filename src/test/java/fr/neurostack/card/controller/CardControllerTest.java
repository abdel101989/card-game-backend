package fr.neurostack.card.controller;

import fr.neurostack.card.model.Card;
import fr.neurostack.card.model.Color;
import fr.neurostack.card.model.DeckOfCards;
import fr.neurostack.card.model.Rank;
import fr.neurostack.card.service.CardService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.annotation.security.RunAs;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Unit tests
 */
@ExtendWith(MockitoExtension.class)
class CardControllerTest {

    @InjectMocks
    CardController cardController;

    @Mock
    private CardService cardService;

    @BeforeEach
    void setUp() {
    }


    @Test
    void getColorsTest() {
        List<Color> colorList = List.of(Color.CARREAUX,Color.COEUR,Color.PIQUE,Color.TREFLE);
        when(cardService.getColors()).thenReturn(colorList);
        List<Color> colors = cardController.getColors();
        assertNotNull(colors);
        List<Color> distinctColors = colors.stream().distinct().collect(Collectors.toList());
        assertEquals(4, distinctColors.size());
    }

    @Test
      void getRanksTest() {
        List<Rank> randomListOfRanks = Rank.getRandomListOfRanks();
        Collections.shuffle(randomListOfRanks);
        when(cardService.getRanks()).thenReturn(randomListOfRanks);
        List<Rank> ranks = cardController.getRanks();
        assertNotNull(ranks);
        assertEquals(13, ranks.size());
        boolean exist = true;
        for (int i = 1; i < 14; i++) {
            exist = exist && ranks.stream().map(rank ->
                    rank.getValue()).collect(Collectors.toList()).contains(i);
        }
        assertTrue(exist);
    }

    @Test
    void getDeckOfCardTest() {
        DeckOfCards deckOfCards = new DeckOfCards(10);
        List<Card> cards = deckOfCards.getCards();
        when(cardService.getDeckOfCard(10)).thenReturn(cards);
        List<Card> deckOfCard = cardController.getDeckOfCard(10);
        assertNotNull(deckOfCard);
        assertEquals(10, deckOfCard.size());
        List<Card> distinctElements = deckOfCard.stream()
                .distinct().collect(Collectors.toList());
        assertEquals(distinctElements,deckOfCard);
    }

}